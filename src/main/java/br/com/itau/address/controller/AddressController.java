package br.com.itau.address.controller;

import br.com.itau.address.client.CepDTO;
import br.com.itau.address.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AddressController {

    @Autowired
    AddressService addressService;

    @GetMapping("/{cep}")
    public CepDTO whereIsDriving(@PathVariable(value = "cep") String cep) {
        CepDTO address = addressService.getAddress(cep);
        return address;
    }
}
