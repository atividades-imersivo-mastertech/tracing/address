package br.com.itau.address.service;

import br.com.itau.address.client.CepClient;
import br.com.itau.address.client.CepDTO;
import br.com.itau.address.client.InvalidCepException;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    @Autowired
    private CepClient cepClient;

    public CepDTO getAddress(String cep) {
        CepDTO address = new CepDTO();

        try {
            address = cepClient.findByCep(cep);
        } catch (FeignException.FeignClientException.BadRequest badRequest) {
            throw new InvalidCepException();
        }

        return address;
    }
}
