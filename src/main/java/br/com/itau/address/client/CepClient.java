package br.com.itau.address.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "viaCep", url = "https://viacep.com.br/ws")
public interface CepClient {
    @GetMapping("/{cep}/json")
    CepDTO findByCep(@PathVariable("cep") String cep);
}
