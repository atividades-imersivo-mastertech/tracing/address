package br.com.itau.address.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "CEP Inválido")
public class InvalidCepException extends RuntimeException {
}
